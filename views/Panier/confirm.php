
<?php 
     ob_start();
       define('PATH',dirname($_SERVER['SCRIPT_FILENAME']));
   ?> 
   <style type="text/css">
   p{padding:0;
   	margin:0;}
 	.footer{
    	width: 100%;
    	
    }
   table{
   	font-size:10px;
   	font-weight:light;
   	border:1px solid #000;
   	border-collapse: collapse;
   	border-spacing: 0;
   }

   </style>
   <page backtop="130px" backbottom="7mm" backleft="0mm" backright="10mm"> 
        <page_header> 
            <table class="page_header">
	            <tr>
	                <td style="width: 32%; text-align: left;padding-left:15mm">
	                    <img src="<?=PATH?>/img/ft/logo.jpg" alt="Logo Sentinel" style="height:16mm">
	                </td>
	                <td style="width: 71.5%;" >
	                    <div class="test" style="font-size:22px;;color:#fff;vertical-align:middle">
	                    	<div style="text-align:right;padding-right:62px;color:#ccc;font-size:50px">
	                    		<b>Devis WEB</b>
	                    	</div>
	                    	
	                    </div>
	                </td>
	            </tr>
	        </table>
        </page_header> 

        <page_footer> 
        	<div class="footer" style="color:#fff;position:relative;vertical-align:middle">
	        	<table class="page_header">
		            <tr>
		                <td style="width: 520px; text-align: left;font-size:8px;color: #8a8c8b;">
		                    S.A.S au capital de 76 225 Euros - RCS Nanterre B328 320 072 - TVA FR 15 328 320 072 - SIRET 328 320 072 00080 - APE 4642 Z
		                </td>
		                <td style="width: 10%;text-align: right;" >
		                    <img src="<?=PATH?>/img/pdf/logofoot.jpg" alt="logo GM" style="">
		                </td>
		            </tr>
		        </table>
        	</div>
        </page_footer> 

        <table>
		    <tr style="width:728px">
	            <td style="width: 45%; text-align: left;vertical-align:top;border:1px solid #000000;padding:5px">
	            	<p><strong>SENTINEL</strong></p>
	            	<p><strong>3 - 5 place du Village</strong></p>
				  	<p><strong>Parc des Barbanniers</strong></p>
				  	<p>92230 Gennevilliers</p>
				  	<p>Tel : 01.34.53.09.88</p>
				  	<p>Fax : 01.39.93.34.92</p>
				  	<p><?=utf8_decode('N°');?> TVA Intracommunautaire FR15328320072</p>
				   
	            </td>
		        <td style="width: 10%;border-right:1px solid #000000"></td>
		        <td style="width: 45%;border:1px solid #000000;padding:2px;vertical-align:top;padding:5px" >
		        	<p><strong><?=utf8_decode('Réference du devis :')?> <?=$tempName?></strong></p>
		        	<p><strong>Client : <?=$user['code_client']?></strong></p>
		        	<p><strong><?=$user['organisme']?></strong></p>
	            	<p><strong><?=$user['code_postal'].'-'.$user['ville']?></strong></p>
				  	<p><strong><?=$user['pays']?></strong></p>
				  	<br>
				  	<p><?=strtoupper($user['nom']).' '.$user['prenom']?></p>
				  	<p><?=$user['mail']?></p>
				  	<p><?=$user['service']?></p>
		        </td>
		    </tr>
		</table><br>
		<table style="border:1px solid #000;position:relative">
			<thead>
				<tr >
					<th style="border:1px solid #000;padding-top:5px;"></th>
					<th style="border:1px solid #000;padding-top:5px;"></th>
					<th style="border:1px solid #000;padding-top:5px;"></th>
					<th style="border:1px solid #000;padding-top:5px;"></th>
					<th style="border:1px solid #000;padding-top:5px;"></th>
					<th style="border:1px solid #000;padding-top:5px;"></th>
				</tr>
				<tr>
					<th style="width:265px;background:#008469;color:#fff;border-left:1px solid #000;border-right:1px solid #000;padding:5px 1px 5px 5px">Article</th>
					<th style="width:85px;background:#008469;color:#fff;border-left:1px solid #000;border-right:1px solid #000;padding:5px 1px 5px 5px">Quantite</th>
					<th style="width:75px;background:#008469;color:#fff;border-left:1px solid #000;border-right:1px solid #000;padding:5px 1px 5px 5px">Prix Brut HT</th>
					<th style="width:45px;background:#008469;color:#fff;border-left:1px solid #000;border-right:1px solid #000;padding:5px 1px 5px 5px">Remise 1</th>
					<th style="width:75px;background:#008469;color:#fff;border-left:1px solid #000;border-right:1px solid #000;padding:5px 1px 5px 5px">Prix Net HT</th>
					<th style="width:95px;background:#008469;color:#fff;border-left:1px solid #000;border-right:1px solid #000;padding:5px 1px 5px 5px">Montant Net HT</th>	
				</tr>
			</thead>
			<tbody>
				
				<?$totals=0;foreach ($articles as $value) {if (strlen($value['code_article']) == 16) {?>
					<tr>
						<th style="width:265px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;"><?=$value['code_article']?> - <?=utf8_decode($value['designation'])?></th>
						<th style="text-align:right;width:65px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=$value['qtt']?></th>
						<th style="text-align:right;width:55px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=$value['prix']?></th>
						<th style="text-align:right;width:25px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px">0</th>
						<th style="text-align:right;width:55px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=$value['prix']?></th>
						<th style="text-align:right;width:75px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=number_format($value['prix']*$value['qtt'],2);$totals+=number_format($value['prix']*$value['qtt'],2);?></th>
					</tr>
				<?}else{
					if (is_null($value['dim1']) || $value['dim1'] == "S0344") {?>
					<tr>
						<th style="width:265px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;"><?=$value['code_article']?> - <?=utf8_decode($value['designation'])?></th>
						<th style="text-align:right;width:65px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=$value['qtt']?></th>
						<th style="text-align:right;width:55px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=$value['prix']?></th>
						<th style="text-align:right;width:25px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px">0</th>
						<th style="text-align:right;width:55px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=$value['prix']?></th>
						<th style="text-align:right;width:75px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=number_format($value['prix']*$value['qtt'],2);$totals+=number_format($value['prix']*$value['qtt'],2);?></th>
					</tr>
					<?}else{if (is_null($value['dim2'])){$value['dim2'] = 'S0344';}?>
					<tr>
						<th style="width:265px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;"><?=$value['code_article'].$value['dim1'].$value['dim2']?> - <?=utf8_decode($value['designation'])?></th>
						<th style="text-align:right;width:65px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=$value['qtt']?></th>
						<th style="text-align:right;width:55px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=$value['prix']?></th>
						<th style="text-align:right;width:25px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px">0</th>
						<th style="text-align:right;width:55px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=$value['prix']?></th>
						<th style="text-align:right;width:75px;padding:5px 1px 5px 5px;border-left:1px solid #000;border-right:1px solid #000;padding-right:10px"><?=number_format($value['prix']*$value['qtt'],2);$totals+=number_format($value['prix']*$value['qtt'],2);?></th>
					</tr>	
					<?}
					}
				}?>
			</tbody>
		</table>
		<table>
			
			<tbody>
				
				<tr style="height:15px">
						<th style="width:265px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:85px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:75px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:45px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:75px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;">Total HT</th>
						<th style="width:114px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;text-align:right;"><?=number_format($totals,2)?> EUR</th>

				</tr>
				<tr style="height:15px">
						<th style="width:265px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:85px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:75px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:45px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:75px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;">TVA</th>
						<th style="width:114px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;text-align:right;">20%</th>

				</tr>
				<tr style="height:15px">
						<th style="width:265px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:85px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:75px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:45px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:75px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;">Frais de port</th>
						<th style="width:114px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;text-align:right;"><?if ($totals <= 400){ echo '15.00 EUR';} else { echo 'Franco de port';}?></th>

				</tr>
				<tr style="height:15px">
						<th style="width:265px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:85px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:75px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:45px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;"></th>
						<th style="width:75px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;">Total TTC</th>
						<th style="width:114px;padding:5px 1px 5px 5px;border-top:1px solid #000;padding-top:5px;text-align:right;"><?$totals = 1.20*$totals; if ($totals <= 400){ $totals += 15;} echo number_format($totals,2)?> EUR</th>

				</tr>

			</tbody>
		</table>

		<table>
			<tr style="height:15px">
				<th style="padding:5px 1px 5px 5px;">Commande <?=utf8_decode('&lt;')?> ou <?=utf8_decode('é')?>gale <?=utf8_decode('à')?> 400 EUR (France m<?=utf8_decode('é')?>tropolitaine) : frais de port de 15 EUR TTC <br>
					Commande > 400 EUR (France m<?=utf8_decode('é')?>tropolitaine) : franco de port <br>
					DOM/TOM : frais de port sur devis
				</th>
			</tr>
		</table>
   
   </page> 
 
   <?php 
       $content = ob_get_clean();

       try{
       $pdf = new HTML2PDF('P','A4','fr', false, 'UTF-8', array(8, 10,8, 10)); 
       $pdf->pdf->SetDisplayMode('fullpage');

      	$pdf->writeHTML($content); 
      	 
      	
      	$pdf->Output(ROOT.'/webroot/uploads/devis/'.$tempName,'F');  

        /*  $pdf->Output();
        /*$pdf->Output('uploads/devis/temp.pdf','F');
     	$pdf->Output('uploads/devis/fichier_test_v2.pdf','');*/
     }catch(HTML2PDF_exception $e){
     	die('Impossible de generer le devis.');
     }
   ?>

<div style="color:#000; text-align:center;padding:10px;border:1px solid #000">
	<p>
		Votre demande de devis a bien été prise en compte. <br>
		Vous serez recontacté(e) au plus vite par <br>notre équipe commerciale.<br>
		Un mail vous a été envoyé.
	</p>
	<br>

	<a href="<?=BASE_URL.'webroot/uploads/devis/'.$tempName?>" target="_blank" style='text-decoration:underline !important;'>Télécharger le devis</a>
</div>

<?
require(ROOT.'/libs/PHPMailer/class.phpmailer.php');
					/*$mail = new PHPMailer();
					try {
					  $mail->From = "site@sentinel.fr";
	   				  $mail->FromName = "Site Sentinel";
					  $mail->Subject = 'Demande de devis';
					  
					  $mail->AltBody = 'Pour voir correctement le message, merci d utiliser le mode html'; // optional - MsgHTML will create an alternate automatically
					  
					  $body = 'Une demande de devis a &eacute;t&eacute; faite sur le site Sentinel.<br>';
					  $mail->MsgHTML($body);
					  $mail->AddAddress('contact@sentinel.fr', "Sentinel");
					  $mail->AddAttachment(ROOT.'/webroot/uploads/devis/'.$tempName);      
					  
					  // Envoi de l'email
						if ( !$mail->Send() ) {
						    die( "Echec de l'envoi du mail, Erreur: " . $mail->ErrorInfo);
						} 
					  
					} catch (phpmailerException $e) {
					  die( $e->errorMessage()); //Pretty error messages from PHPMailer
					} catch (Exception $e) {
					  die( $e->getMessage()); //Boring error messages from anything else!
					}
*/

$mail = new PHPMailer();
					try {
					  $mail->From = "contact@sentinel.fr";
	   				  $mail->FromName = "Contact site  Sentinel";
					  $mail->Subject = 'Demande de devis sur le site Sentinel';
					  
					  $mail->AltBody = 'Pour voir correctement le message, merci d utiliser le mode html'; // optional - MsgHTML will create an alternate automatically
					  
					  $body = 'Vous avez fait une demande de devis sur le site Sentinel.fr. Ci joint votre demande de devis en pi&egrave;ce jointe<br>';
					  $mail->MsgHTML($body);
					  $mail->AddAddress($user['mail'], "Sentinel");
					  $mail->AddAttachment(ROOT.'/webroot/uploads/devis/'.$tempName);      
					  
					  // Envoi de l'email
						if ( !$mail->Send() ) {
						    die( "Echec de l'envoi du mail, Erreur: " . $mail->ErrorInfo);
						} 
					  
					} catch (phpmailerException $e) {
					  die( $e->errorMessage()); //Pretty error messages from PHPMailer
					} catch (Exception $e) {
					  die( $e->getMessage()); //Boring error messages from anything else!
					}

unset($_SESSION['panier']);
?>