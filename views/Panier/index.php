
<div class="content">
	<ul class="ulnone">
		<li class="inbl fondarticle_liste_menu vat ">
			<img src="<?=WEB?>img/temp.jpg" alt="">
			<div class="titreclient">Appel d'offres</div>
			
			<div >
				<?$temp = '';
				if (!empty($lots)) {
					foreach ($lots as $k => $lot) {
						if ($lot->marche != $temp) {?>
							<div class="marche">Marché &nbsp;&nbsp;<strong><?=$lot->marche;?></strong></div>
						<?}?>

						<?if($lot->lot == $lotactif){?>
							<div class="lot lotactif"><?=$lot->lot;?></div>
						<?}else{?>
							<a href="<?=BASE_URL?>Espace_client/lot/<?=$lot->lot;?>" class="lot " ><?=$lot->lot;?></a>
						<?}?>
							
					<?$temp = $lot->marche;}

				}else{?>
					<div class="marche">Pas de Marché en cours</div>
				<?}?>
				<div class="clear"></div>
			</div>
			<div class="seePanier"><a href="<?=BASE_URL?>Panier"><img src="<?=WEB?>img/iconpanier.png" alt=""> Voir le panier</a></div>
		</li><!--  
		 --><li class="inbl list_article_container rel">
		 <div class="searchmenuliste">
		 	<a href="<?=BASE_URL?>Ft/listes"  target="_blank" class="inbl btnsearchmore vat left">Listes des fiches Techniques</a>
			<a href="<?=BASE_URL?>Recherche/avancee" class="inbl btnsearchmore vat"><i class="fa fa-search"></i> Recherche avancée</a>
			<form action="<?=BASE_URL?>Recherche/" class="inbl vat" method="get">
				<input type="text" class="searchinput" name="searchterm" id="searchterm" required><!--   
				--><button type="submit" value="OK" class="btnsubmitsearch">OK</button>

			</form>
		</div>
		<div class="etapes"><img src="<?=WEB?>img/etape2.png"></div>
		 <div class="retour rel good"><img src="<?=WEB?>img/retour.png"><a href="<?=BASE_URL?>"><<< ACCUEIL</a></div>
		
		 <div class="containerarticle rel">
		<?/*echo '<pre>';
		print_r($articles);
		echo '</pre>';*/?>
		<?if (isset($articles) && $articles != false) {?>
				<table  class="tableLot">
									<thead>
										<tr>
											<th class="">Description</th>
											<th class="">Référence</th>
											<th class="">Prix</th>
											<th class="">Dim.1</th>
											<th class="">Dim.2</th>
											<th class="">Qté.</th>
											<th >Total</th>
										</tr>
									</thead>
									<tbody>
									<?foreach ($articles as $key => $article) {?>
										<tr class="artpanier" id="<?=$key?>">
											<td class="titretab " id="<?=$article['designation']?>"><a href="<?=BASE_URL.$article['url_menu']?>/<?=$article['id_article'].'-'.$article['ref_url_article']?>" class="titretaba"><?=ucfirst(strtolower($article['designation']))?></a></td>
											<td class="catab"><?=$article['code_article']?></td>
											<td class="r">
											<?if(isset($article['prixsanspromo'])){?>
												<span style="text-decoration : line-through;color:#f00">&nbsp;<?=$article['prixsanspromo'];?> </span>&nbsp;&nbsp;<span class="rexact"><?=$article['prix'];?></span>
											<?}else{?>
												<span class="rexact"><?=$article['prix']?></span>
											<?}?>

											</td>
											<td class="d">
												<?if (!empty($article['dimensions1'])) {?>
													<select class="dimensiontab" id="<?=$article['dim2'];?>">
														<?foreach ($article['dimensions1'] as $dim) {?>
															<option value="<?=$article['id_article'].'-'.$dim->catdim1.$dim->dim1?>" <?if($article['dim1'] == $dim->catdim1.$dim->dim1){echo 'selected';}?>><?=$dim->dim1l?></option>
														<?}?>
													</select>
												<?}?>
												
											</td>
											<td class="dim2tab">
												<?if (!empty($article['dimensions2'])) {?>
													<select class="dimensiontab2" >
														<?foreach ($article['dimensions2'] as $dim) {?>
															<option value="<?=$article['id_article'].'-'.$dim->catdim2.$dim->dim2?>" <?if($article['dim2'] == $dim->catdim2.$dim->dim2){echo 'selected';}?>><?=$dim->dim2l?></option>
														<?}?>
													</select>
												<?}?>
											</td>
											<td class="qtttabcontainer" style="width:62px;padding:2px">
												<span class="qtttab vat"><?=$article['qtt']?></span>
												<div class="inbl vat">
													<div class="ajoutqtttab noselect">+</div>
													<div class="enleveqtttab noselect">-</div>
												</div>
												<span style="margin-left:2px">
													<a href="<?=BASE_URL?>Panier/deleteqttpanier/<?=$key?>" class="titretaba confirm"><i class="fa fa-times"></i></a>
												</span>
											</td>
											
											<td class="total" style="text-align:right"></td>
										</tr>
									<?}?>
									<tr><td></td></tr>
									<tr>
										<td colspan="6" style="text-align:right">TOTAL</td>
										<td class="totals" style="text-align:right"></td>
									</tr>
									</tbody>
								</table>
		<?}else{?>
			<h3>Pas d'article dans le panier</h3>
		<?}?>
				<div class="nextPanier">
					<a href="<?=BASE_URL?>Espace_client"><img src="<?=WEB?>img/iconpanier.png" alt=""> Etape Précédente</a>
					<?php if (!empty($_SESSION['panier'])): ?>
						<a href="<?=BASE_URL?>Panier/validation"><img src="<?=WEB?>img/iconpanier.png" alt=""> Etape Suivante</a>
					<?php endif ?>
				</div>
			</div>

		</li>
	</ul>

</div>

<script type="text/javascript">
	$(function(){
		var total = 0;
		$('.artpanier').each(function(){
			var prix = $(this).find('.rexact').text();
			var qtt =  $(this).find('.qtttab').text();

			var sum = prix * qtt;
			$(this).find('.total').text(sum.toFixed(2)); 
			
			total = total + sum;
			$('.totals').text(total.toFixed(2));
		})



		$('.ajoutqtttab').click(function(event) {
			var x = $(this).parent().prev().text();
			x++;
			$(this).parent().prev().text(x);

			var prix = $(this).parent().parent().siblings('.r').children('.rexact').text();
			console.log(prix);
			var sum = prix * x;
			$(this).parent().parent().siblings('.total').text(sum.toFixed(2)); 

			
			total = parseFloat(total) + parseFloat(prix);
			$('.totals').text(total.toFixed(2));

			//Actualisation de la qtt de l article en session
			var key = $(this).parent().parent().parent().attr('id');
			$.ajax({

				url: '<?=BASE_URL?>Panier/ajoutqttpanier',
				type: 'POST',
				data : {'qtt' : x, 'key' : key},
				success : function(code_html, statut){ 
					
      			}
			})

		});
		$('.enleveqtttab').click(function(event) {
			var x = $(this).parent().prev().text();
			if (x>1) {
				x--;
			$(this).parent().prev().text(x);

			var prix = $(this).parent().parent().siblings('.r').children('.rexact').text();
			
			var sum = prix * x;
			$(this).parent().parent().siblings('.total').text(sum.toFixed(2)); 

			total = parseFloat(total) - parseFloat(prix);
			$('.totals').text(total.toFixed(2));
			};


			//Actualisation de la qtt de l article en session
			var key = $(this).parent().parent().parent().attr('id');
			$.ajax({

				url: '<?=BASE_URL?>Panier/enleveqttpanier',
				type: 'POST',
				data : {'qtt' : x, 'key' : key},
				success : function(code_html, statut){ 
					
      			}
			})
		});



		//Recuperation et affichage des 2eme dimensions si existent
		$(".dimensiontab").change(function(){
			var dim1 = $(this).val();
			var t = $(this);
			var dim2 = $(this).attr('id');
			
			var key = $(this).parent().parent().attr('id');
			//Recuperation de la dimension 2 si existe
			$.ajax({
				url: '<?=BASE_URL?>Pages/recupdimlot',
				type: 'POST',
				data : {'dim1' : dim1 ,'dim2' : dim2},
				success : function(code_html, statut){ 
					$(t).parent().siblings('.dim2tab').html(code_html);

					var x = $(t).parent().siblings('.dim2tab').find('.dim2').val();
					
					$.ajax({
						url: '<?=BASE_URL?>Pages/changedim2panier',
						type: 'POST',
						data : {'dim2' : x, 'key' : key},
						success : function(code_html, statut){ 
		      			}
      				})
				}
			})


			//Modification si besoin du prix par rapport a la dim1
			var ca = $(this).parent().siblings('.catab').text();
			
			
			$.ajax({
				url: '<?=BASE_URL?>Pages/recupprixdim',
				type: 'POST',
				data : {'dim1' : dim1 ,'code_article' : ca, 'key' : key},
				success : function(code_html, statut){ 
					var x = $(t).parent().siblings('.r');
					$(t).parent().siblings('.r').children('.rexact').text(code_html);
					
					var prix =code_html;
					var qtt =  $(x).siblings('.qtttabcontainer').find('.qtttab').text();

					
					var sum = prix * qtt;
					$(x).siblings('.total').text(sum.toFixed(2));


					var tot = 0;
					$('.artpanier').each(function(){
						var tota = $(this).find('.total').text();
						
						tot = parseFloat(tot) + parseFloat(tota);
						$('.totals').text(tot.toFixed(2));
					})


      			}
			})
		})

		$(".dimensiontab2").change(function(){
			var dim2 = $(this).val();
			var t = $(this);
			var key = $(this).parent().parent().attr('id');

			console.log(key);
			$.ajax({
				url: '<?=BASE_URL?>Pages/recupdimpanier',
				type: 'POST',
				data : {'dim2' : dim2 ,'key' : key},
				success : function(code_html, statut){ 


					var x =  $(".dimensiontab2").val();
					console.log(x);
					$.ajax({
						url: '<?=BASE_URL?>Pages/changedim2panier1',
						type: 'POST',
						data : {'dim2' : x, 'key' : key},
						success : function(code_html, statut){ 
		      			}
      				})
      			}
			})
		})

		
	})
</script>
