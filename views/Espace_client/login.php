<div class="content">
	<div class="connexion">
		<img src="<?=WEB?>img/fondconnexion.png " alt="">
		<div>
			<form action="<?=BASE_URL?>Espace_client/login" method="post">
				<div class="connexionForm">
					<p class="good">Accès sécurisé</p>
					<p>Veuillez entrer vos codes d'accès et cliquer sur le bouton "Accéder" afin d'entrer dans la boutique. <i><a href="<?=BASE_URL?>Inscription">Cliquez ici</a> si vous ne disposez pas encore d'un accès.</i></p>
					
					<ul class="reset ulconnexion">
						<li class="inbl mr30">
							<label>Nom d'utilisateur (email)</label><br>
							<input type="text" name="name" id="name" required>
						</li>
						<li class="inbl">
							<label>Mot de Passe</label><br>
							<input type="password" name="pwd" id="pwd" required> 
						</li>
					</ul>
					
					<div style="text-align: right; padding-right:15px; padding-top:5px;">
						<a href="<?=BASE_URL?>Espace_client/reset" >Mot de passe oublié ?</a>
					</div>
				
				</div>
				
				<div class="espaceclient good">
					<input type="submit" name="submit" value="Accéder &nbsp;>>>">
				</div>
			</form>
		</div>
	</div>
	
	<div class="espaceclientContainer">
		<img src="<?=WEB?>img/btnclient.png" alt="">
	</div>
	
</div>
