
<div class="content">
	<ul class="ulnone">
		<li class="inbl fondarticle_liste_menu vat ">
			<img src="<?=WEB?>img/temp.jpg" alt="">
			<div class="titreclient">Appel d'offres <!-- <?=$client->raison_social_1?> --></div>
			<!--<div class="logoclient"> <img src="<?=WEB?>img/logotest.jpg" alt=""> </div>-->
			<div >
				<?$temp = '';
				if (!empty($lots)) {
					foreach ($lots as $k => $lot) {
						if ($lot->marche != $temp) {?>
							<div class="marche">Marché &nbsp;&nbsp;<strong><?=$lot->marche;?></strong></div>
						<?}?>

						<?if($lot->lot == $lotactif){?>
							<div class="lot lotactif"><?=$lot->lot;?></div>
						<?}else{?>
							<a href="<?=BASE_URL?>Espace_client/lot/<?=$lot->lot;?>" class="lot " ><?=$lot->lot;?></a>
						<?}?>
							
					<?$temp = $lot->marche;}

				}else{?>
					<div class="marche">Pas de Marché en cours</div>
				<?}?>
				<div class="clear"></div>
			</div>
			<div class="etapes"><img src="<?=WEB?>img/etape1.png"></div>
			<div class="seePanier"><a href="<?=BASE_URL?>Panier"><img src="<?=WEB?>img/iconpanier.png" alt=""> Voir le panier</a></div>
		</li><!--  
		 --><li class="inbl list_article_container rel">
		 <div class="searchmenuliste">
			<a href="<?=BASE_URL?>Recherche/avancee" class="inbl btnsearchmore vat"><i class="fa fa-search"></i> Recherche avancée</a>
			<form action="<?=BASE_URL?>Recherche/" class="inbl vat" method="get">
				<input type="text" class="searchinput" name="searchterm" id="searchterm" required><!--   
				--><button type="submit" value="OK" class="btnsubmitsearch">OK</button>

			</form>
		</div>
		 <div class="retour rel good"><img src="<?=WEB?>img/retour.png"><a href="<?=BASE_URL?>"><<< ACCUEIL</a></div>
		 <div class="containerarticle rel">
		<?if (isset($articles) && $articles != false) {?>
				<table id="table" class="tablesorter tableLot">
									<thead>
										<tr>
											<th class="">Description</th>
											<th class="">Référence</th>
											<th class="filter-false">Prix</th>
											<th class="filter-false">Dim.1</th>
											<th class="filter-false">Dim.2</th>
											<th class="filter-false">Qté.</th>
											<th class="filter-false">Photos</th>
											<th class="filter-false">Fiche</th>
											<th class="filter-false">Panier</th>
										</tr>
									</thead>
									<tbody>
									<?foreach ($articles as $article) {?>
										<tr class="pagination">
											<td class="titretab"  id="<?=$article->id_article?>"><a class="titretaba" href="<?=BASE_URL.$article->url_menu?>/<?=$article->id_article.'-'.$article->ref_url_article;?>"><?=ucfirst(strtolower($article->designation))?></a></td>
											<td class="catab"><?=$article->code_article?></td>
											<td class="r"><?=$article->prix?></td>
											<td class="d">
												<?if (!empty($article->dimension)) {?>
													<select class="dimensiontab" style="width:50px">
														<option value="null"></option>
														<?foreach ($article->dimension as $dim) {?>
															<option value="<?=$article->id_article.'-'.$dim->catdim1.$dim->dim1?>"><?=$dim->dim1l?></option>
														<?}?>
													</select>
												<?}?>
												
											</td>
											<td class="dim2tab">
												<div class="txt">
												</div>
											</td>
											<td class="qtttabcontainer">
												<span class="qtttab vat">1</span>
												<div class="inbl vat">
													<div class="ajoutqtttab noselect">+</div>
													<div class="enleveqtttab noselect">-</div>
												</div>
											</td>
											<td>
												<?if (!empty($article->imgs)) {?>
													<a href="<?=WEB?>uploads/images/big/<?=$article->imgs->url_image?>" class="group4"><img src="<?=WEB?>img/photos.png" alt=""></a>
												<?}else{?>
													Pas de photo
												<?}?>
											</td>
											<td><a href="<?=BASE_URL?>FicheTechnique/<?=$article->code_article?>-<?=$article->ref_url_article?>" target='blank'><img src="<?=WEB?>img/fiche.jpg" alt=""></a></td>
											<td class="ajoutPaniertab"><div >Ajouter au panier</div></td>
										</tr>
									<?}?>
									</tbody>
								</table>
		<?}else{
			echo 'Pas d\'information pour la page <strong>'.strtoupper($name2).'</strong> de la catégorie <strong>'.strtoupper($name).'</strong>';
		}?>
		<div class="nextPanier"><a href="<?=BASE_URL?>Panier"><img src="<?=WEB?>img/iconpanier.png" alt=""> Etape Suivante</a></div>
		
		</div>
		</li>
	</ul>


</div>



<script type="text/javascript">
$(function(){
	//Increment decremente la quantite
		$('.ajoutPaniertab').click(function(event) {
			var aa = $(this).siblings('.d').find('.dimensiontab');
			if (aa.size() == 0) {
					var id = $(this).siblings('.titretab').attr('id');
					var ca = $(this).siblings('.catab').html();
					var qtt =  $(this).siblings('.qtttabcontainer').find('.qtttab').text();;
					
					$.ajax({
						url: '<?=BASE_URL?>Panier/ajoutclientsansdim',
						type: 'POST',
						data : {'code_article':ca,'qtt': qtt, 'id' : id},
						success : function(code_html, statut){ 
		           			$('body').append('<div class="successPanier">Article ajouté au panier</div>');
		      			}
					})
			};
		});

		$('.ajoutqtttab').click(function(event) {
			var x = $(this).parent().prev().text();
			x++;
			$(this).parent().prev().text(x);
		});
		$('.enleveqtttab').click(function(event) {
			var x = $(this).parent().prev().text();
			if (x>1) {
				x--;
			$(this).parent().prev().text(x);
			};
		});

		//Recuperation et affichage des 2eme dimensions si existent
		$(".dimensiontab").change(function(){
			var dim1 = $(this).val();
			var t = $(this);
			$(this).parent().siblings('.ajoutPaniertab').unbind("mouseenter");
			$(this).parent().siblings('.ajoutPaniertab').unbind("click");

			
			if (dim1 != "null") {	

				$(this).parent().siblings('.ajoutPaniertab').click(function(event) {
					var cadim = dim1;
					var ca = $(this).siblings('.catab').html();
					var dim2 = $(this).siblings('.dim2tab').find('.dim2').val();
					var qtt =  $(this).siblings('.qtttabcontainer').find('.qtttab').text();;
					
					$.ajax({
						url: '<?=BASE_URL?>Panier/ajoutclient',
						type: 'POST',
						data : {'code_article':ca,'qtt': qtt, 'dimension' : cadim, 'dim2' : dim2},
						success : function(code_html, statut){ 
		           			$('body').append('<div class="successPanier">Article ajouté au panier</div>');
		      			}
					})
				});
			}else{
				$(this).parent().siblings('.ajoutPaniertab').mouseenter(function(e){
					$('body').append('<div class="selectPanier">Vous devez choisir une dimension</div>')
		      			
				})
				$('.ajoutPaniertab').mouseleave(function(e){
					$('.selectPanier').remove()	
				})
			};

			
			$.ajax({
				url: '<?=BASE_URL?>Pages/recupdimlot',
				type: 'POST',
				data : {'dim1' : dim1},
				success : function(code_html, statut){ 
					$(t).parent().siblings('.dim2tab').html(code_html);
      			}
			})
		})
		
		//Affichage du texte d autorisation ou non de l ajout au panier a l init de la page
		$('.dimensiontab').each(function(){

			if ($(this).val() == "null") {	
				$(this).parent().siblings('.ajoutPaniertab').mouseenter(function(e){
					$('body').append('<div class="selectPanier">Vous devez choisir une dimension</div>')
		      			
				})
				$('.ajoutPaniertab').mouseleave(function(e){
					$('.selectPanier').remove()	
				})
			}
		})


		
		
})
</script>

