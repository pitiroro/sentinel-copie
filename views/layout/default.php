<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Sentinel <?= isset($title_for_layout)? utf8_encode($title_for_layout):'';?></title>
	
	<meta name="Content-Type" content="UTF-8">
	<meta name="Content-Language" content="fr">
	<meta http-equiv="Cache-control" content="public">

	<meta name="Robots" content="all">
	<meta name="google-site-verification" content="vFp7rz7JZZa2KXBMx77zi3TGAVHMm_hrB2F-H3J2eKg" />
	<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
	
	<meta name="keywords" content="<?=isset($keywords)?utf8_encode($keywords):'Sentinel, Uniformes, équipements, asvp, police, police municipale, PM, sécurité école, garde champêtre, collectivités'?>" />
	<meta name="Description" content="<?=isset($descriptionPage)?utf8_encode($descriptionPage):'Sentinel, habillement et équipement des forces de sécurité territoriale.'?>" />

	<link rel="icon" type="image/x-icon" href="<?=WEB?>img/favicon.ico" />
<link rel="shortcut icon" href="<?=WEB?>img/favicon.ico" type="image/x-icon">

	<link rel="stylesheet" href="<?=WEB?>css/css/font-awesome.min.css" />


	<link rel="stylesheet" href="<?=LIB?>normalize-css/normalize.css" media="all">
	<link rel="stylesheet" href="<?=WEB?>css/css/main.css" media="all">
	<link rel="stylesheet" href="<?=WEB?>css/css/styleFlash.css" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?=WEB?>js/slick/slick.css"/>

	<script src="<?=WEB?>js/jquery.js" type="text/javascript"></script>
	<script src="<?=WEB?>js/jqueryui/jquery-ui.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?=LIB?>elevatezoom-master/jquery.elevateZoom-3.0.8.min.js"></script>

	<link rel='stylesheet' type='text/css' href='<?=LIB?>jquery.dynatree-1.2.6-all/dist/skin-vista/ui.dynatree.css'>
	<script type="text/javascript" src="<?=LIB?>jquery.dynatree-1.2.6-all/dist/jquery.dynatree.min.js"></script>
	<link rel="stylesheet" href="<?=LIB?>colorbox-master/colorbox.css" media="all">

	<script src="<?=LIB?>sweetalert/lib/sweet-alert.min.js"></script> 
	<link rel="stylesheet" type="text/css" href="<?=LIB?>sweetalert/lib/sweet-alert.css">
	
	<!--[if lt IE 9]>
	<script src="<?=WEB?>js/css3-mediaqueries.js"></script>
	<script src="<?=WEB?>js/html5shiv.js"></script>
	<link rel="stylesheet" href="<?=WEB?>css/css/mainIe8.css" type="text/css" media="screen" /> 
	<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59177133-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>

<div class="wrapper">
<noscript style="color:#f00;font-weight:bold;font-size:20px;margin-left:200px;">Javascript n'est pas activé. Activez le et recharger la page pour naviguer sur le site.</noscript>
	<div class="wrap_padding">

		<!-- HEADER -->
		<div class="header">
			<div class="logo">
				<a href="<?=BASE_URL?>"><img src="<?=WEB?>img/logo.jpg" alt="logo Sentinel"></a>
			</div>

			<?if(isset($client)){?>
			<div class="topespaceclient">
				<a href="<?=BASE_URL?>Panier" class="hide <?if (!empty($_SESSION['panier'])) {echo 'showB';}?>" id="iconpanier"><i class="fa fa-shopping-cart"></i></a>
				<?if (isset($_SESSION['user'])) {?>
					<span>
						<a href="<?=BASE_URL?>Espace_client"><?=ucfirst($_SESSION['user']['organisme']);?></a>
						<?if(!empty($_SESSION['panier'])){?>
							<!-- <a href="<?=BASE_URL?>Panier"><i class="fa fa-shopping-cart"></i></a> -->
						<?}?>
					</span>
					
					<a href="<?=BASE_URL?>Espace_client/logout" class="cont"><i class="fa fa-user"></i> Déconnexion</a>
				<?}else{?>
					<a href="<?=BASE_URL?>Espace_client" class="cont"><i class="fa fa-user"></i> Espace client</a>
				<?}?>
			</div>
			<?}?>

			<div class="menubg">
				<img src="<?=WEB?>img/test.png" alt="barre de fond du menu">
			</div>
			
			<div class="menu_container">
				<?$i = 0;$path = str_replace(BASE_URL, '', $_SERVER['REQUEST_URI']);
				 foreach ($menu as $menu) {?>
					<span><a href="<?=BASE_URL.$menu->url_rubrique?>" class="menu <?if( $path == $menu->url_rubrique ){echo 'menuactive';}?>" ><?=ucfirst($menu->lib_rubrique)?></a></span>
				<?$i++;}?>
			</div>
		</div>


		<!-- CONTENT -->
		<div class="container">
			<!-- Affiche le contenu des pages --> 
			<?php echo $content_for_layout; ?>
		</div>

	</div>
	
	
	<!-- FOOTER -->
	<div class="footer">
		
		
		<div class="lignFoot alignR ">
			<div class="menufoot">
				<?$i = 0; foreach ($foot as $foot) {?>
					<?if($i != 0){?><span class="blue">-</span><?}?>
					<span class="blue  <?if($i == 0){echo "lh22 p5";}?>"><a href="<?=BASE_URL?><?=$foot->url_rubrique?>"><?=ucfirst($foot->lib_rubrique)?></a></span>
				<?$i++;}?>
			</div>
			<div class="accroche">
				<a href="http://www.groupemarck.fr" target="_blank"><strong>SENTINEL, un savoir-faire du Groupe Marck</strong></a>
			</div>
			
		</div>
		
		

		


	</div>
	<div class="flashContent">
		<?$msg = new Messages();$msg->display();?>
	</div>
</div>


<?if (empty($_SESSION['user'])) {?>
	
<div id="connecPanier" style="display:none">
	
	
	<div  style="padding: 15px 20px;position:absolute;top:30%;left:40%;margin:auto;width:410px;background:#fff;border:1px solid #000;color:#000;text-align:center">
		<strong>Site réservé aux professionels</strong><br><br>
		<ul style="list-style: none;margin:0;padding:0">
			<li style="display: inline-block; margin-right:20px;">
			Vous êtes déjà client <br>
			<a href="<?=BASE_URL?>Espace_client/login" style="text-decoration: underline;">Se connecter</a>
			</li>
			<li style="display: inline-block;">
				Vous n'êtes pas client <br>
				<a href="<?=BASE_URL?>Inscription" style="text-decoration: underline;">Créer un compte</a>
			</li>
		</ul>
		<br>
		<div style="font-size:11px;">* Ce site, réservé aux professionnels, n'est pas un site marchand,il ne permet pas <br>de commander nos produits mais uniquement de générer une demande de devis</div>
	<div id="closeconnec" style="position:absolute;top:0;right:4px;"><i class="fa fa-times"></i></div>
	</div>
</div>

<?}?>


<!-- <div class="lignbleu"></div>
<div class="lignbleuT"></div>
<div class="lignblanc"></div> -->



<script type="text/javascript" src="<?=WEB?>js/slick/slick.min.js"></script>
<script type="text/javascript" src="<?=LIB?>tablesorter-master/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="<?=LIB?>tablesorter-master/js/jquery.tablesorter.widgets.min.js"></script>


<script type="text/javascript" src="<?=LIB?>colorbox-master/jquery.colorbox-min.js"></script>

<script>
    $(function(){
    	$('#diap-titre-0').addClass('show');
	 	$('#diap-text-0').addClass('show');
	 	$('#diap-menu-0').addClass('menudiapoactif');
		$('.slide').slick({
	 	 	autoplay:true,
	 	 	speed: 600,
	 	 	autoplaySpeed: 6000,
	 	 	pauseOnHover:false,
	 	 	onAfterChange: function(slide, index){
	 	 		var t = $(".slide").slickCurrentSlide();
	 	 		/*alert (t);*/
	 	 		$('.show').removeClass('show');
	 	 		$('#diap-titre-' + t).addClass('show');
	 	 		$('#diap-text-' + t).addClass('show');
	 	 		$('.menudiapoactif').removeClass('menudiapoactif');
	 	 		$('#diap-menu-' + t).addClass('menudiapoactif');


	 	 	}
	 	 	/*
	 	 	asNavFor: '.slider_titre',
	 	 	asNavFor: '.slider_text'*/
		})

		$(".group4").colorbox(/*{rel:'group4', slideshow:true}*/);
		$('body').delegate('.fancybox', 'click', function(){
				$(this).colorbox({
					
				});
			});

		$(".fancyboxcb").colorbox({
					onCleanup:function(){window.location.href = '<?=BASE_URL?>Panier';},
					onClosed:function(){window.location.href = '<?=BASE_URL?>Panier'; }
				});

		$('#closeconnec').click(function(event) {
			$('#connecPanier').hide();
		});
		$('#closeconnecins').click(function(event) {
			$('#connecPanierins').hide();
		});

		/*$('#table').tablesorter({
			    theme: 'blue',
			    widthFixed : true,
			    widgets: ["zebra", "filter"],
			    ignoreCase: false,
			    widgetOptions : {
			    	 filter_childRows : false,
			    	 filter_columnFilters : true,
			    	 filter_cssFilter : 'table-search',
			    	 filter_filteredRow   : 'filtered',
			    	 filter_formatter : null,
			    	 filter_functions : null,
			    	 filter_hideFilters : true,
			    	 filter_ignoreCase : true,
			    	 filter_liveSearch : true,
			    	 filter_onlyAvail : 'filter-onlyAvail',
			    	 filter_reset : 'button.reset',
			    	 filter_saveFilters : true,
			    	 filter_searchDelay : 300,
			    	 filter_serversideFiltering: false,
			    	 filter_startsWith : false,
			    	 filter_useParsedData : false,
			    	 filter_defaultAttrib : 'data-value',
			      // filter_anyMatch replaced! Instead use the filter_external option
			      // Set to use a jQuery selector (or jQuery object) pointing to the
			      // external filter (column specific or any match)
			      	filter_external : '',
			      
			      filter_placeholder: { search : 'Search...' },
			    
			    }
			  });*/


			function confirm(href){
				
				swal({   
						title: "Voulez vous vraiment supprimer?",     
						type: "warning",   
						showCancelButton: true,   
						confirmButtonColor: "#DD6B55",   
						confirmButtonText: "Oui, supprimer!" 
					}, function(){   
						/*alert(href); */
						window.location.href = href;
						
					});
			}
			
			$('.confirm').click(function(){ confirm(this.href); return false; });

	});
</script>

</body>
</html>