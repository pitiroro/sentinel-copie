
<div class="content">
	<div class="diapo">
		<div class="slide">
			<?foreach ($menudiaps as $menudiap) {
				if ($menudiap->diapo==1) {?>
					<div><img src="<?=WEB?>uploads/diapo/<?=$menudiap->url_img_diapo?>" alt="<?=$menudiap->url_menu?>"></div>
				<?}?>
			<?}?>
		</div>
		

		<div class="contentdiapo">
			<div class="titlediapo">
				<!-- <p class="good">SÉCURITÉ TERRITORIALE</p> -->
				<div class="slider_titre">
					<?$i=0;foreach ($menudiaps as $menudiap) {
						if ($menudiap->diapo==1) {?>
							<div id="diap-titre-<?=$i?>" class="hide good"><?=ucfirst($menudiap->lib_menu)?></div>
						<?};$i++;?>
					<?}?>
				</div>
			</div>
			<div class="slider_text textediapo ">
				<?$i=0;foreach ($menudiaps as $menudiap) {
					if ($menudiap->diapo==1) {?>
						<div id='diap-text-<?=$i?>' class="hide"><?=$menudiap->text_diap?></div>
					<?};$i++;?>
				<?}?>
			</div>
			<div class="menudiapoContainer">
				<?$i=0;foreach ($menudiaps as $menudiap) {?>
					<a href="<?=BASE_URL.$menudiap->url_menu?>" class="menudiapo" id="diap-menu-<?=$i?>"><?=ucfirst($menudiap->lib_menu)?></a>
				<?$i++;}?>
			</div>

			<div class="search">
				<div class="searchmenu">
					<a href="<?=BASE_URL?>Recherche/avancee" class="inbl btnsearchmore vat"><i class="fa fa-search"></i> Recherche avancée</a>
					<form action="<?=BASE_URL?>Recherche/" class="inbl vat" method="get">
						<input type="text" class="searchinput" name="searchterm" id="searchterm" required><!--   
						--><button type="submit" value="OK" class="btnsubmitsearch">OK</button>

					</form>
				</div>
			</div>

			
		</div>
	</div>
	
	<div class="espaceclientContainer">
		<img src="<?=WEB?>img/btnclient.jpg" alt="Barre espace client">
		<div id="clig" style="position:absolute;right:0;top:0;width:305px;height:115px;padding-top:4px;z-index:500;">
			<div id="cligcontent">
				<?=html_entity_decode($actu->content_actu);?>
			</div>
		</div>
	</div>
	<!-- <div class="espaceclientContainer">
		<img src="<?=WEB?>img/btnclient.png" alt="Barre espace client">
	</div>
	<div class="espaceclient good">
			<a href="<?=BASE_URL?>Espace_client">ESPACE CLIENT &nbsp;>>></a>
		</div> -->
</div>

