<div class="content_acceuil">
	<div class="rel">
		<img src="<?=WEB?>uploads/menus/<?=$info->url_img?>" alt="">

		<div class="mt10 mb30">
			<?foreach ($menudiaps as $menudiap) {
				if($menudiap->url_menu == $name){?>
					<a  class="menuacceuilactif"><?=ucfirst($menudiap->lib_menu)?></a>
				<?}else{?>
					<a href="<?=BASE_URL.$menudiap->url_menu?>" class="menuacceuil"><?=ucfirst($menudiap->lib_menu)?></a>
				<?}?>
			<?}?>
		</div>
	

		<div class="contentdiapo">
			<div class="titleacceuil">
				<div class="slider_titre" style="margin-top:0;">
					<div class="good"><?=ucfirst($info->lib_menu)?></div>
				</div>
				<!-- <p >Sécurité Territoriale</p> -->
			</div>
			
			<div class="searchacceuil">
				<div class="searchmenu">
					<a href="<?=BASE_URL?>Recherche/avancee" class="inbl btnsearchmore vat"><i class="fa fa-search"></i> Recherche avancée</a>
					<form action="<?=BASE_URL?>Recherche/" class="inbl vat" method="get">
						<input type="text" class="searchinput" name="searchterm" id="searchterm" required><!--   
						--><button type="submit" value="OK" class="btnsubmitsearch">OK</button>

					</form>
				</div>
			</div>

			<div class="ssmenuacceuil">
				<?foreach ($ssmenus as $ssmenu) {?>
					<a href="<?=BASE_URL.$ssmenu->url_menu?>"><?=ucfirst($ssmenu->lib_menu)?></a>
				<?}?>
			</div>
		</div>
	</div>
</div>



