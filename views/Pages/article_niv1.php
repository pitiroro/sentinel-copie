<?
$path = str_replace(BASE_URL, '', $_SERVER['REQUEST_URI']);
$pos = strrpos($path, '/');
$temp = substr($path, $pos);
$res = str_replace($temp, '', $path);
?>
<div class="content">
	<ul class="ulnone">
		<li class="inbl fondarticle_menu vat">
			<a href="<?=BASE_URL?>"><span class="good seecollect"><<<&nbsp;&nbsp;Voir les collections</span></a>
		</li><!--  
		 --><li class="inbl list_article_container rel">
		  <div class="containerarticle"><!--
		<?if (isset($articles) && $articles != false) {
			foreach ($articles as $article) {?>
				--><div class="w4 vat"><a href="<?=$_SERVER['REQUEST_URI'].'/'.$article->id_article.'-'.$article->ref_url_article?>">
					<?if ($article->opt_new == 1) {?>
						<!-- <div class="new">Nouveauté</div> -->
						<img src="<?=WEB?>img/new.png" alt="Nouveauté" style="position:absolute;top:2px">
					<?}?>
					<div class="img">
						<?if($article->url_img != false){?>
							<img src="<?=WEB?>uploads/images/liste/<?=$article->url_img?>">
						<?}else{?>
							<img src="<?=WEB?>uploads/images/liste/no_image.jpg">
						<?}?>
						
					</div>
					<div class="titre"><?=$article->designation?></div>
					<div class="ref">REF. <?=$article->code_article?></div>
					<div class="desc_courte"><?if(isset($article->desc_courte)){ echo $article->desc_courte;}else{echo '<p>Pas de description</p>';}?></div>
					<div class="prix">
						<?if ($article->opt_ondemand == 1) {?>
							Produit à la demande
						<?}else{?>
							<?=number_format(($article->prix*(1+($tva->taux_tva/100))),2);?>€ TTC
						<?}?>
					</div>
					<div class="details"><a href="<?=$_SERVER['REQUEST_URI'].'/'.$article->id_article.'-'.$article->ref_url_article?>">Details <i class="fa fa-play"></i></a></div>
				</div></a><!--
			<?}
			
		}else{
			echo '--><div>Pas d\'article pour la catégorie <strong>'.strtoupper($name).'</strong></div><!--';
		}?>--></div>
		</li>
	</ul>

	<div class="mt10 mb30">
			<?foreach ($menudiaps as $menudiap) {
				if($menudiap->url_menu == $name){?>
					<a  class="menuacceuilactif"><?=ucfirst($menudiap->lib_menu)?></a>
				<?}else{?>
					<a href="<?=BASE_URL.$menudiap->url_menu?>" class="menuacceuil"><?=ucfirst($menudiap->lib_menu)?></a>
				<?}?>
			<?}?>
		</div>
</div>



