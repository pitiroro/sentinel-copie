<?
if ($_SERVER['HTTP_HOST'] == 'localhost') {
	$path = str_replace(BASE_URL, '', $_SERVER['REQUEST_URI']);
}else{
	$path =  $_SERVER['REQUEST_URI'];
}
$pos = strrpos($path, '/');
$temp = substr($path, $pos);
if ($_SERVER['HTTP_HOST'] == 'localhost') {
	$res = str_replace($temp, '', $path);
	$chemin = str_replace($temp, '', $path);
}else{
	$res1 = str_replace($temp, '', $path);
	$res1 = substr($res1, 1);
	$res = 'http://www.sentinel.fr'.str_replace($temp, '', $path);
	$chemin = '.'.str_replace($temp, '', $path);
}

$pos = strrpos($res, '/');
$temp = substr($res, $pos);
$temp = str_replace('/', '', $temp);

?>
<div class="content">
	<ul class="ulnone">
		<li class="inbl fondarticle_liste_menu vat ">
			<img src="<?=WEB?>img/fondtest.jpg" alt="">
				<?foreach ($ssmenus as $k => $ssmenu) {?>
					<?if (is_array($ssmenu[1])) {?>
						<div class="fondssmenuli">
							<a href="<?=BASE_URL.$ssmenu[0]?>" class="ssmenuliactif"><?=$k?></a>
							<ul class='ulnone categ'>
								<?foreach ($ssmenu[1] as $value) {
									if ($value->url_menu == $res1) {$titreCateg = $value->lib_menu;?>
										<li class="categliactif"><a href="<?=BASE_URL.$value->url_menu?>" ><?=$value->lib_menu?></a></li>
									<?}else{?>
										<li class="categli"><a href="<?=BASE_URL.$value->url_menu?>" ><?=$value->lib_menu?></a></li>
									<?}?>
								<?}?>
							</ul>
							<div class="clear"></div>
						</div>
					<?}else{?>
					
						<a href="<?=BASE_URL.$ssmenu[0]?>" class="ssmenuli"><?=$k?></a>
					
					<?}
				}?>
		</li><!--  
		 --><li class="inbl list_article_containerproduit rel">
				<div class="searchmenuliste">
					<div class="good left arianne">
						<a href="<?=BASE_URL.$chemin?>"><?=str_replace('-', ' ', $univers)?></a>
					</div>
					<a href="<?=BASE_URL?>Recherche/avancee" class="inbl btnsearchmore vat"><i class="fa fa-search"></i> Recherche avancée</a>
					<form action="<?=BASE_URL?>Recherche/" class="inbl vat" method="get">
						<input type="text" class="searchinput" name="searchterm" id="searchterm" required><!--   
						--><button type="submit" value="OK" class="btnsubmitsearch">OK</button>

					</form>
				</div>

				<div class="retour">
					<img src="<?=WEB?>img/retour.png">
					<div class="titre">
							<a href="<?=BASE_URL.$chemin?>" class="titre"><?=ucfirst($temp)?></a>
					</div>
				</div>

				<div class="containerarticleproduit">
				<?if (isset($article) && $article != false) {?>
					<div class="top">
						<div class="titre inbl tab">
							<?=ucfirst(strtolower($article->designation));?>
						</div><!-- 
						--><div class="fichetech inbl vat"><a href="<?=BASE_URL?>FicheTechnique/<?=$article->code_article?>-<?=$article->ref_url_article?>" target='blank'><img src="<?=WEB?>img/iconfiche.png" alt="" > Fiche Technique</a></div><!--
						--><div class="prix inbl tab">
							<?if ($article->opt_ondemand == 1) {?>
								Prix sur demande
							<?}else{?>
								<?=number_format(($article->prix*(1+($tva->taux_tva/100))),2);?>€ TTC
							<?}?>
							</div>
					</div>
					<div class="middle">
						<ul class="ulnone">
							<li class="inbl vat"  >
								<?if (!empty($images)) {?>
									<img id="zoom_01" src="<?=WEB?>uploads/images/<?=$images[0]->url_image?>" alt="" data-zoom-image="<?=WEB?>uploads/images/big/<?=$images[0]->url_image?>">
								<?}else{?>
									<img id="no_zoom" src="<?=WEB?>uploads/images/no_image.jpg" alt="">
								<?}?>
							</li><!--   
							--><li class="inbl vat infoarticle">
								<div class="topinfoarticle">
									<img class='floatR' src="<?=WEB?>img/ajoutdevis.png" alt="">
									<div class="ajout">Ajouter au devis</div>
								</div>
								<div class="c&q">
									<div class="couleur inbl vat">
										<?if (!empty($couleurs)) {
											foreach ($couleurs as $couleur) {
												if ($couleur->img == true) {?>
													<div class="color" style="background:<?=$couleur->code_hexa?>" id="<?if(isset($couleur->dim1)){echo $couleur->dim1;}elseif(isset($couleur->dim2)){echo $couleur->dim2;}?>">
														<span><?=utf8_encode($couleur->nom_couleur)?></span>
													</div>
												<?}else{?>
													<div class="colorN" style="background:<?=$couleur->code_hexa?>">
														<span class="rel ">
															<?=utf8_encode($couleur->nom_couleur)?>
															<div>Pas d'image disponible pour cette couleur</div>
														</span>
														
													</div>
												<?}?>
												
											<?}?>
											<?if (!empty($images)) {?>
												<div class="colorreset noselect" >
													Defaut
												</div>
											<?}?>
										<?}?>


										<?if (!empty($links)) {
											foreach ($links as $couleur) {?>
												<a href="<?=$res.'/'.$couleur[0]->id_article.'-'.$couleur[0]->ref_url_article?>">
													<div class="color" style="background:<?=$couleur[0]->code_hexa?>;color:#325f8d;" >
														<span><?=utf8_encode($couleur[0]->couleur)?></span>
													</div>
												</a>
												
												
											
											<?}?>
										<?}?>
									</div><!--   
									--><div class="inbl vat">
										<div class="qttcontainer">
											<div class="titre">Quantité</div>
											<div class="qtt">
												<span id="qtt">1</span>
												<span class="ajoutqtt noselect">+</span>
												<span class="enleveqtt noselect">-</span>
											</div>
										</div>
										<div class="tailles">
											<?if (!empty($dim1s)) {?>
												<div class="titre"><?=$dim1s[0]->catdim1l?></div>
												<select name="dim1" id="dim1">
													<option value="null"></option>
													<?foreach ($dim1s as $key => $value) {?>
														<option value="<?=$value->catdim1?><?=$value->dim1?>"><?=$value->dim1l?></option>
													<?}?>
												</select>
											<?}?>
											
											
										</div>
									</div>
								</div>
								<div class="visuel ">
									<?if (!empty($images)) {
										foreach ($images as $image) {?>
												<img id="<?=$image->url_image?>" src="<?=WEB?>uploads/images/thumb/<?=$image->url_image?>" alt="" class="inbl imgpetit">
										<?}?>
									<?}?>
								</div>
								<div class="desc">
									<?if (!empty($article->desc_courte)) {?>
										<?=ucfirst(htmlspecialchars_decode($article->desc_longue));?>
									<?}else{?>
										Pas de description pour <strong><?=htmlspecialchars_decode($article->designation)?></strong>
									<?}?>
									
								</div>
							</li>

						</ul>
					</div>
					
					
				<?}else{
					echo 'Pas d\'information pour la page <strong>'.strtoupper($name2).'</strong> de la catégorie <strong>'.strtoupper($name).'</strong>';
				}?>

					<div class="retourcateg">
						<img src="<?=WEB?>img/retourcateg.png" alt="">
						<div class="back"><a href="<?=$res?>">Revenir aux <?=ucfirst($temp);?></a></div>

						<div class="position">
							<?$i=1; foreach ($nbr as $key => $value) {
								if($value['id'] == $id_article){
									if ($i-2 >=0) {?>
										<a href="<?=$res.'/'.$nbr[$i-2]['url']?>" class="prev vat"></a>
									<?}else{?>
										<div class="prevV"></div>
									<?}?>
									<div class="nbr vat ">
										<strong>Article <?echo $i;?></strong>/<?=count($nbr)?>
									</div>
									<?if ($i+1 <= count($nbr)) {?>
										<a href="<?=$res.'/'.$nbr[$i]['url']?>" class="next vat"></a>
									<?}else{?>
										<div class="prevV"></div>
									<?}?>
									
								<?};
								$i++;
							}?>
						</div>
					</div>
					
					
					<?if (isset($vcs)) {?>
						<p class="pvc">Les produits qui peuvent vous intéresser</p>
						<div><!--   
							<?foreach ($vcs as $vc) {?>
								--><div class="wvc vat">
									<div class='titrevc'><?=$vc->designation?> - <?=$vc->code_article?></div>
									<ul class="ulnone">
										<li class="inbl imgvc vat">
											<?if (isset($vc->imagevc)) {?>
												<img src="<?=WEB?>uploads/images/thumb/<?=$vc->imagevc?>" alt="" class="inbl" style="width:83px">
											<?}else{?>
												<img src="<?=WEB?>img/no_imagesmall.jpg" alt="" class="inbl">
											<?}?>
										</li><!--
										--><li class="inbl descvc vat"><?=$vc->desc_courte?></li>
									</ul>
									<div class="seevc">
										<a href="<?=BASE_URL.$vc->url_menu.'/'.$vc->id_article.'-'.$vc->ref_url_article?>">Voir la fiche &nbsp;<i class="fa fa-play"></i></a>
									</div>
								</div><!-- 
							<?}?>
							
						--></div>
					<?}?>
					
				</div>
		</li>
	</ul>


	<div class="mt10 mb30">
			<?foreach ($menudiaps as $menudiap) {
				if($menudiap->url_menu == $univers){?>
					<a  class="menuacceuilactif"><?=ucfirst($menudiap->lib_menu)?></a>
				<?}else{?>
					<a href="<?=BASE_URL.$menudiap->url_menu?>" class="menuacceuil"><?=ucfirst($menudiap->lib_menu)?></a>
				<?}?>
			<?}?>
		</div>
</div>
<script>
	$(function(){
		$("#zoom_01").elevateZoom({
			zoomWindowPosition: 9, 
			zoomWindowOffetx: -5
		});


		//Increment decremente la quantite
		var i=1;

		$('.ajoutqtt').click(function(event) {
			i++;
			$('.qtt #qtt').text(i)
		});
		$('.enleveqtt').click(function(event) {
			if (i>1) {
				i--;
			$('.qtt #qtt').text(i)
			};
		});

		//Recuperation et affichage des 2eme dimensions si existent
		$("#dim1").change(function(){
			var dim1 = $('#dim1').val();

			$('.ajout').unbind("mouseenter");
			$('.ajout').unbind("click");

			console.log(dim1);
			if (dim1 != "null") {	

				$('.ajout').click(function(event) {
					var dim1 = $('#dim1').val();
					var dim2 = $('#dim2').val();
					var qtt = i;
					var ca = '<?=$article->code_article?>';

					$('#iconpanier').addClass('showB');

					$.ajax({
						url: '<?=BASE_URL?>Panier/ajout',
						type: 'POST',
						data : {'id_article':<?=$id_article?>,'code_article':ca,'qtt': qtt, 'dim1' : dim1, 'dim2' : dim2},
						success : function(code_html, statut){ 
		           			$('body').append('<div class="successPanier">Article ajouté au panier</div>')
		      			}
					})		
				});
			}else{
				$('.ajout').mouseenter(function(e){
					$('body').append('<div class="selectPanier">Vous devez choisir une <?=$catdim1l?></div>')
		      			
				})
				$('.ajout').mouseleave(function(e){
					$('.selectPanier').remove()	
				})
			};


			$.ajax({
				url: '<?=BASE_URL?>Pages/recupdim',
				type: 'POST',
				data : {'id_article':<?=$id_article?>,'dim1' : dim1},
				success : function(code_html, statut){ 
					if($(".tailles2").size() >= 1) {
						
						$('.tailles2').html( code_html);
					}else{
						$('.tailles').append('<div class="tailles2">'+ code_html +'</div>');
					}
           			
      			}
			})
		})
		
		//Affichage du texte d autorisation ou non de l ajout au panier
		var aa = $("#dim1") ;
		if (aa.size() > 0) {
			if ($('#dim1').val() != "null") {	
				$('.ajout').click(function(event) {
					var dim1 = $('#dim1').val();
					var dim2 = $('#dim2').val();
					var qtt = i;
					var ca = '<?=$article->code_article?>';

					$('#iconpanier').addClass('showB');

					$.ajax({
						url: '<?=BASE_URL?>Panier/ajout',
						type: 'POST',
						data : {'id_article':<?=$id_article?>,'code_article':ca,'qtt': qtt, 'dim1' : dim1, 'dim2' : dim2},
						success : function(code_html, statut){ 
		           			$('body').append('<div class="successPanier">Article ajouté au panier</div>')
		      			}
					})		
				});
			}else{
				$('.ajout').mouseenter(function(e){
					$('body').append('<div class="selectPanier">Vous devez choisir une <?=$catdim1l?></div>')
		      			
				})
				$('.ajout').mouseleave(function(e){
					$('.selectPanier').remove()	
				})
			};
		}else{
			
			$('.ajout').click(function(event) {
					var dim1 = $('#dim1').val();
					var dim2 = $('#dim2').val();
					var qtt = i;
					var ca = '<?=$article->code_article?>';

					$('#iconpanier').addClass('showB');
					
					$.ajax({
						url: '<?=BASE_URL?>Panier/ajout',
						type: 'POST',
						data : {'id_article':<?=$id_article?>,'code_article':ca,'qtt': qtt, 'dim1' : dim1, 'dim2' : dim2},
						success : function(code_html, statut){ 
		           			$('body').append('<div class="successPanier">Article ajouté au panier</div>')
		      			}
					})		
				});
		};
		       

		//Affichage du texte au survol des couleurs
		$('.color').mouseenter(function(e){
			$(this).find('span').css(
				'display', 'block'
			);	
		})
		$('.color').mouseleave(function(e){
			$(this).find('span').css(
				'display', 'none'
			);	
		})
		$('.colorN').mouseenter(function(e){
			$(this).find('span').css(
				'display', 'block'
			);
			$(this).find('div').css(
				'display', 'block'
			);		
		})
		$('.colorN').mouseleave(function(e){
			$(this).find('span').css(
				'display', 'none'
			);	
			$(this).find('div').css(
				'display', 'none'
			);		
		})  


		
		//Changement de la grande image au click sur les petites
		$('.imgpetit').click(function(e){
			var url = $(this).attr('id');
			
			console.log(url);
			url1 = '<?=WEB?>uploads/images/'+url;
			urllarge = '<?=WEB?>uploads/images/big/'+url;
			
			$('#zoom_01').attr('src', url1 );
			$('.zoomWindowContainer > div').attr('style','background-image:url("'+urllarge+'");overflow: hidden; text-align: center; width: 400px; height: 400px; float: left; display: none; z-index: 100; border: 4px solid rgb(136, 136, 136); position: absolute; background-size: 1000px 1000px;background-position: 0px 0px; background-repeat: no-repeat;');
		}) 

		//Changement des images au click sur la couleur
		$('.color').click(function(event) {
			var dim = $(this).attr('id');

			$.ajax({
				url: '<?=BASE_URL?>Pages/images',
				type: 'POST',
				data : {'id_article':<?=$id_article?>,'dim':dim},
				success : function(code_html, statut){ 
		          		$('#zoom_01').attr('src', code_html );
		      	}
			})	

			$.ajax({
				url: '<?=BASE_URL?>Pages/imagespetit',
				type: 'POST',
				data : {'id_article':<?=$id_article?>,'dim':dim},
				success : function(code_html, statut){ 
		          		$('.visuel ').html(code_html );
		      	}
			})	
		});

		$('.colorreset').click(function(event) {
		

			$.ajax({
				url: '<?=BASE_URL?>Pages/images',
				type: 'POST',
				data : {'id_article':<?=$id_article?>,'dim':''},
				success : function(code_html, statut){ 
		          		$('#zoom_01').attr('src', code_html );
		      	}
			})	

			$.ajax({
				url: '<?=BASE_URL?>Pages/imagespetit',
				type: 'POST',
				data : {'id_article':<?=$id_article?>,'dim':''},
				success : function(code_html, statut){ 
		          		$('.visuel ').html(code_html );
		      	}
			})	
		});
	})
</script>
