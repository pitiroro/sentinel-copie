<?php 
if ($_SERVER['HTTP_HOST'] == 'localhost') {
	define('BASE_URL',dirname(dirname($_SERVER['SCRIPT_NAME'])).'/');
}else{
	define('BASE_URL','http://www.sentinel.fr/');
}

 ?>
 <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Sentinel</title>
		
	<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">

	<link rel="icon" type="image/x-icon" href="../webroot/img/favicon.ico" />

	<link rel="stylesheet" href="../libs/normalize-css/normalize.css" media="all" />
	<link rel="stylesheet" href="../webroot/css/css/login.css" />
	<link rel="stylesheet" href="../webroot/css/css/font-awesome.min.css" />
	<link rel="stylesheet" href="../webroot/css/css/styleFlash.css" type="text/css" media="all" />

	<!--[if lt IE 9]>
	<script src="../webroot/js/html5shiv.js"></script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="logo-login">
			<h1 class="h1-title">SENTINEL</h1>
			
		</div>
		<div class="panel-bg">
			<div class="panel">
				<div class="panel-heading">
					<span><i class="fa fa-lock lock border-right mr5"></i>Locked</span>
					<span class="right-0">Activez votre inscription ?</span>
				</div>
				<form action="<?=BASE_URL?>Inscription/mdp" method="post">
					<div class="panel-body">
						<p class="blue">Choississez un mot de passe :</p>
						<ul>
							<li class="input-gr inline"><i class="fa fa-link"></i></li><!--   
							--><li class="inline vam"><input type="password" name="pwd" id="pwd" class="form-input" placeholder="Mot de passe"></li>
						</ul>
					</div>
					<div class="panel-footer">
						<input type="hidden" name="uid" value="<?=$_GET['uid']?>">
						<input type="hidden" name="mail" value="<?=$_GET['m']?>">
						<input type="submit" value="Login" class="btn">
					</div>
					
				</form>
						
				<div class="flash-message">
					
				</div>
			</div>
		</div>
		<div class="txtcenter copyright">© Sentinel 2014</div>
	</div>
	<script src="../webroot/js/jquery.js" type="text/javascript"></script>
	<script src="../webroot/js/jquery.backstretch.min.js" type="text/javascript"></script>
	<script src="../webroot/js/jquery.velocity.min.js" type="text/javascript"></script>
	<script>
		(function($){
			$('.panel-bg ,.copyright').velocity({
				opacity: [1,0],
				translateY: [0,100]
			},{
				easing: [60,10],
				duration:800
			})
			
		})(jQuery);
		
	</script>
</body>
</html>
