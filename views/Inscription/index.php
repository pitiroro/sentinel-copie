<div class="content">
	<div class="connexion">
		<img src="<?=WEB?>img/fondconnexion.png " alt="">
		<div>
			<form action="<?=BASE_URL?>Inscription/send" method="post">
				<div class="inscriptionForm">
					<p class="good">INSCRIPTION</p>
					<p>(Les champs marqués d'un * sont obligatoires)</p>

					<ul class="reset ulinscription">
						<li class="inbl mr30">
							<label>Civilité *</label><br>
							<select name="civilite" id="civilite" required>
								<option value="Mr">Mr</option>
								<option value="Mme">Mme</option>
							</select>
						</li>
						<li class="inbl">
							<label>Grade</label><br>
							<input type="text" name="grade" id="grade"> 
						</li>
					</ul>
					<ul class="reset ulinscription">
						<li class="inbl mr30">
							<label>Prénom *</label><br>
							<input type="text" name="prenom" id="prenom" required>
						</li>
						<li class="inbl">
							<label>Nom *</label><br>
							<input type="text" name="nom" id="nom" required> 
						</li>
					</ul>
					<ul class="reset ulinscription">
						<li class="inbl mr30">
							<label>Organisme / Société *</label><br>
							<input type="text" name="organisation" id="organisation" required>
						</li>
						<li class="inbl">
							<label>Service</label><br>
							<input type="text" name="service" id="service"> 
						</li>
					</ul>
					<ul class="reset ulinscription">
						<li class="inbl mr30">
							<label>Adresse e-mail *</label><br>
							<input type="email" name="mail" id="mail" required>
						</li>
						<li class="inbl">
							<label>Fonction</label><br>
							<input type="text" name="fonction" id="fonction"> 
						</li>
					</ul>
					<ul class="reset ulinscription">
						<li class="inbl mr30">
							<label>Adresse *</label><br>
							<input type="text" name="adresse" id="adresse" required>
						</li>
						<li class="inbl">
							<label>Complement d'adresse</label><br>
							<input type="text" name="compadresse" id="compadresse"> 
						</li>
					</ul>
					<ul class="reset ulinscription">
						<li class="inbl mr30">
							<label>Code Postal *</label><br>
							<input type="text" name="codepostal" id="codepostal" required>
						</li>
						<li class="inbl">
							<label>Téléphone</label><br>
							<input type="tel" name="tel" id="tel" > 
						</li>
					</ul>
					<ul class="reset ulinscription">
						<li class="inbl mr30">
							<label>Ville *</label><br>
							<input type="text" name="ville" id="ville" required>
						</li>
						<li class="inbl">
							<label>Fax</label><br>
							<input type="tel" name="fax" id="fax" > 
						</li>
					</ul>
					
				</div>
				
				<div class="espaceclient good">
					<input type="submit" name="submit" value="S'inscrire &nbsp;>>>">
				</div>
			</form>
		</div>
	</div>
	
	<div class="espaceclientContainer">
		<img src="<?=WEB?>img/btnclient.png" alt="">
	</div>
	
</div>
<div id="connecPanierins">
	<div style="width:100%;height:100%;top:0;left:0;position:absolute;z-index:3"></div>
	<div  style="padding: 15px 20px;position:absolute;z-index:4;top:35%;left:30%;margin:auto;width:460px;background:#fff;border:1px solid #000;color:#000;text-align:center">
		<strong>Site réservé aux professionnels</strong><br><br>
		
		<div style="font-size:14px">Les demandes de login sont <strong>réservées aux municipalités <br> et Polices Municipales </strong>pour établir leurs devis <br>et /ou accéder à leurs marchés en cours. <br><br> Toute autre demande ne sera pas prise en compte.</div>
		<div id="closeconnecins" style="position:absolute;top:0;right:4px;cursor:pointer"><i class="fa fa-times"></i></div>
	</div>
</div>
